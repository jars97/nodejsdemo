var db = require('../dbconnection'); 
var Bank = {  
    getAll: function(callback) {  
        return db.query("Select * from bancos", callback);  
    },
    get: function(id, callback) {  
        return db.query("select * from bancos where id=?", [id], callback);  
    },
    add: function(Bank,callback){
        db.query("insert into bancos (descripcion,abreviatura) values (?,?)",[Bank.descripcion,Bank.abreviatura],callback);
    },
    delete:function(id,callback){
        return db.query("delete from bancos where Id=?",[id],callback);
    },
    update:function(id,Bank,callback){
        return db.query("update bancos set descripcion=?,abreviatura=? where Id=?",[Bank.descripcion,Bank.abreviatura],callback);
    }
};  
module.exports = Bank;  