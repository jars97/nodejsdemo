var express = require('express');  
var router = express.Router();  
var Obj = require('../models/Banks'); 

router.get('/:id?', function(req, res, next) {  
    if (req.params.id) {  
        Obj.get(req.params.id, function(err, rows) {  
            if (err) {  
                res.json(err);  
            } else {  
                //res.json({"Error" : false, "Message" : "Success", "Users" : rows});
                res.json(rows[0]);  
            }  
        });  
    } else {  
        Obj.getAll(function(err, rows) {  
            if (err) {  
                res.json(err);  
            } else {  
                res.json(rows);  
            }  
        });  
    }  
}); 
router.post('/',function(req,res,next){
    Obj.add(req.body,function(err,count){
      if(err){
        res.json(err);
      }
      else{
        res.json(req.body);
        //or return count for 1 &amp;amp;amp; 0
      }
    });
 });
 router.delete('/:id', function(req, res, next) {  
    Obj.delete(req.params.id, function(err, count) {  
        if (err) {  
            res.json(err);  
        } else {  
            res.json(count);  
        }  
    });  
});  
router.put('/:id', function(req, res, next) {  
    Obj.update(req.params.id, req.body, function(err, rows) {  
        if (err) {  
            res.json(err);  
        } else {  
            res.json(rows);  
        }  
    });  
});
module.exports = router;